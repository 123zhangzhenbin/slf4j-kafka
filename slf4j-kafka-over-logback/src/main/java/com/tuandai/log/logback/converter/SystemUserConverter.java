package com.tuandai.log.logback.converter;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * 获取系统用户
 * Created by zhangzhenbin on 17-5-24.
 */
public class SystemUserConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        return System.getProperty("user.name");
    }
}
