package com.tuandai.log.logback.converter;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.tuandai.log.kafka.config.Slf4jKafkaConfig;
import com.tuandai.log.kafka.config.Slf4jKafkaContext;

/**
 * 获取配置文件中的APPID
 * Created by zhangzhenbin on 17-5-24.
 */
public class AppIdConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        Slf4jKafkaConfig contextSlf4jKafkaConfig = null;
        try {
            contextSlf4jKafkaConfig = Slf4jKafkaContext.getContextSlf4jKafkaConfig();
            return contextSlf4jKafkaConfig.getAppId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
