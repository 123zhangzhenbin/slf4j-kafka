package com.tuandai.log.logback.appender;

import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import com.tuandai.log.kafka.producer.KafkaProducerConcurrentProcessor;

/**
 * 提供异步插入，不保证数据顺序
 * Created by zhangzhenbin on 17-5-25.
 */
public class KafkaConcurrentAppender<E> extends KafkaAppender<E> {
    public KafkaConcurrentAppender() throws Exception {
        super();
    }
    /**
     * This method differentiates RollingFileAppender from its super class.
     */
    @Override
    protected void subAppend(E event) {
        LayoutWrappingEncoder<E> encoder = (LayoutWrappingEncoder<E>) this.getEncoder();
        String msg = encoder.getLayout().doLayout(event);
        KafkaProducerConcurrentProcessor.send(slf4jKafkaConfig,msg);
    }
}
