package com.tuandai.log.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zhangzhenbin on 17-5-24.
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            logger.info("10 /\""+i+"\"");
        }
    }
}
