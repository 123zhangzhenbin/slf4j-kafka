package com.tuandai.log.logback.layout;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import com.tuandai.log.logback.converter.AppIdConverter;
import com.tuandai.log.logback.converter.EncodedMessageConverter;
import com.tuandai.log.logback.converter.ProcessIdConverter;
import com.tuandai.log.logback.converter.SystemUserConverter;

/**
 * Created by zhangzhenbin on 17-5-24.
 */
public class ExtendedPatternLayoutEncoder extends PatternLayoutEncoder {
    @Override
    public void start() {
        // put your converter
        PatternLayout.defaultConverterMap.put(
                "pid", ProcessIdConverter.class.getName());
        PatternLayout.defaultConverterMap.put(
                "app_id", AppIdConverter.class.getName());
        PatternLayout.defaultConverterMap.put(
                "sys_user", SystemUserConverter.class.getName());
        PatternLayout.defaultConverterMap.put(
                "encode_message", EncodedMessageConverter.class.getName());
        super.start();
    }
}
