package com.tuandai.log.logback.converter;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.tuandai.log.kafka.config.Slf4jKafkaConfig;
import com.tuandai.log.kafka.config.Slf4jKafkaContext;

/**
 * 去掉消息中的单双引号
 * Created by zhangzhenbin on 17-5-24.
 */
public class EncodedMessageConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        return event.getFormattedMessage().replace("\"","\\\"").replace("'","\\'");
    }
}
