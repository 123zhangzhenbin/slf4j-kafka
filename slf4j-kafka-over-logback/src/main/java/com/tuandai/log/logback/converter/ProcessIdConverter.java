package com.tuandai.log.logback.converter;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * ProcessIdConverter
 * Created by zhangzhenbin on 17-5-24.
 */
public class ProcessIdConverter extends ClassicConverter {
    private static final String PROCESS_ID =
            new ApplicationPid().toString();

    @Override
    public String convert(final ILoggingEvent event) {
        // for every logging event return processId from mx bean
        // (or better alternative)
        return PROCESS_ID;
    }
}
