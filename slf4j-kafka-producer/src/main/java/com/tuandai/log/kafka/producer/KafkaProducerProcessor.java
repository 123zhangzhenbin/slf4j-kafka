package com.tuandai.log.kafka.producer;

import com.tuandai.log.kafka.config.Slf4jKafkaConfig;
import com.tuandai.log.kafka.config.Slf4jKafkaContext;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 将消息推送到kafka的处理器
 * Created by zhangzhenbin on 17-5-24.
 */
public class KafkaProducerProcessor {

    public static void send(Slf4jKafkaConfig slf4jKafkaConfig, String msg){
        KafkaProducer kafkaProducer = KafkaProducerContext.getKafkaProducer();
        KafkaSenderHandler.getInstance(kafkaProducer).send(new ProducerRecord(slf4jKafkaConfig.getKafkaTopic(),msg));
    }
    public static void init(Slf4jKafkaConfig slf4jKafkaConfig){
        KafkaProducerContext.init(slf4jKafkaConfig);
    }

}
