package com.tuandai.log.kafka.producer;

import com.tuandai.log.kafka.config.Slf4jKafkaConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * 将消息推送到kafka的处理器，请求非序列化
 * Created by zhangzhenbin on 17-5-24.
 */
public class KafkaProducerConcurrentProcessor {

    public static void send(Slf4jKafkaConfig slf4jKafkaConfig, String msg){
        KafkaProducer kafkaProducer = KafkaProducerContext.getKafkaProducer();
        kafkaProducer.send(new ProducerRecord(slf4jKafkaConfig.getKafkaTopic(),msg));
    }
    public static void init(Slf4jKafkaConfig slf4jKafkaConfig){
        KafkaProducerContext.init(slf4jKafkaConfig);
    }

}
