package com.tuandai.log.log4j12.layout;

import com.tuandai.log.log4j12.parser.EnhancedPatternParser;
import org.apache.log4j.EnhancedPatternLayout;

/**
 * Created by zhangzhenbin on 17-5-25.
 */
public class ExtendedEnhancedPatternLayout extends EnhancedPatternLayout {
    @Override
    protected org.apache.log4j.helpers.PatternParser createPatternParser(String pattern) {
        return new EnhancedPatternParser(pattern);
    }
}
