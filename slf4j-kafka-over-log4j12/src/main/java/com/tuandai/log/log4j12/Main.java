package com.tuandai.log.log4j12;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log4j 1.2.X 测试主类
 * Created by zhangzhenbin on 17-5-25.
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) {
        logger.info("log4j just \"123\"test");
    }
}
