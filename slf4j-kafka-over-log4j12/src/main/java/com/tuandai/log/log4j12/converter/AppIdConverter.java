package com.tuandai.log.log4j12.converter;

import com.tuandai.log.kafka.config.Slf4jKafkaConfig;
import com.tuandai.log.kafka.config.Slf4jKafkaContext;
import org.apache.log4j.pattern.DatePatternConverter;
import org.apache.log4j.pattern.FullLocationPatternConverter;
import org.apache.log4j.pattern.LoggingEventPatternConverter;
import org.apache.log4j.spi.LoggingEvent;

/**
 * 获取配置文件中的APPID
 * Created by zhangzhenbin on 17-5-24.
 */
public class AppIdConverter extends LoggingEventPatternConverter {
    private static final AppIdConverter INSTANCE =
            new AppIdConverter();

    private AppIdConverter() {
        super("APP ID", "appId");
    }
    public static AppIdConverter newInstance(
            final String[] options) {
        return INSTANCE;
    }

    @Override
    public void format(LoggingEvent event, StringBuffer toAppendTo) {
        try {
            Slf4jKafkaConfig contextSlf4jKafkaConfig = Slf4jKafkaContext.getContextSlf4jKafkaConfig();
            toAppendTo.append(contextSlf4jKafkaConfig.getAppId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
