package com.tuandai.log.log4j12.converter;

import org.apache.log4j.pattern.LoggingEventPatternConverter;
import org.apache.log4j.spi.LoggingEvent;

/**
 * ProcessIdConverter
 * Created by zhangzhenbin on 17-5-24.
 */
public class ProcessIdConverter extends LoggingEventPatternConverter {
    private static final ProcessIdConverter INSTANCE =
            new ProcessIdConverter();

    private static final String PROCESS_ID =
            new ApplicationPid().toString();

    protected ProcessIdConverter() {
        super("application process pid", "processId");
    }

    public static ProcessIdConverter newInstance(
            final String[] options){
        return INSTANCE;
    }
    
    @Override
    public void format(LoggingEvent event, StringBuffer toAppendTo) {
        toAppendTo.append(PROCESS_ID);
    }
}
